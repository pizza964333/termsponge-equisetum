
import System.Environment
import qualified Data.ByteString as B
import Debug.Trace

main = do
  [arg1,arg2] <- getArgs 
  d <- readFile arg1
  let e = lines d :: [String]
  mapM_ print e
  a <- B.readFile arg2
  let c = B.pack $ patch e $ B.unpack a
  B.writeFile (arg2 ++ ".patched") c

test [] = []
test o@(0x48:0xc7:0xc7:a) = 0x48 : 0xc7 : 0xc7 : traceShow (take 7 o) (test a)
test (a:b) = a : test b

patch [] a = a
patch _ a@[_,_,_,_,_,_] = a
patch (n:m) a | take 7 a == pat = traceShow pat (patch m (pat2 ++ drop 7 a))
 where
  hexn = read ("0x"++n) :: Integer
  hex1 = fromInteger $ hexn               `mod` 256
  hex2 = fromInteger $ hexn `div`  256    `mod` 256
  hex3 = fromInteger $ hexn `div` (256^2) `mod` 256
  hex4 = fromInteger $ hexn `div` (256^3) `mod` 256
  pat2 = [0x48,0xc7,0xc7,hex1,hex2,hex3,hex4]

patch n (a:b) = a : patch n b

pat = [0x48,0xc7,0xc7,0xff,0x00,0xff,0x00]

  
