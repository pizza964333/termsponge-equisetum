module TermNetElement where

import Data.Int
import Data.Word
import Foreign.Marshal.Array
import Foreign.Storable
import Foreign.Ptr
import System.IO.Unsafe
import Control.Monad.Trans.Reader
import System.Random
import Control.Monad
import Control.Monad.IO.Class
import Data.Bits
import TNETypeBits

data TermNetElement

newtype TermNetAddr = TNA Int
 deriving (Read,Show,Eq,Ord)

type TNA = TermNetAddr

instance Storable TermNetElement where
  sizeOf    _ = 8
  alignment _ = 8

type TNIO a = ReaderT (Ptr TermNetElement) IO a

newTermNetPool :: IO (Ptr TermNetElement) -- 8 Megabytes will allocated
newTermNetPool = callocArray $ 2^20

runTNIO :: Ptr TermNetElement -> TNIO a -> IO a
runTNIO ptr tnio = runReaderT tnio ptr

newTermNetElement :: Integer -> TNIO TermNetAddr
newTermNetElement typeBits = do
  a <- ask
  b <- liftIO $ randomRIO (0,2^20)
  let c = a `advancePtr` b
  let d = castPtr c :: Ptr Word64
  e <- liftIO $ peek d
  if e /= 0 then newTermNetElement typeBits
            else do
              liftIO $ poke d $ fromInteger typeBits
              return $ TNA b

getTNElementAsWord64 :: TNA -> TNIO Word64
getTNElementAsWord64 (TNA a) = do
  b <- ask
  let c = b `advancePtr` a
  let d = castPtr c :: Ptr Word64
  liftIO $ peek d

tneGetTypeString :: TNA -> TNIO String
tneGetTypeString a = do
  b <- getTNElementAsWord64 a
  let c = b .&. (2^20-1)
  return $ typeBitsToString c
  

tneSetLeftAddr  :: TNA -> TNA -> TNIO ()
tneSetLeftAddr = undefined

tneSetRightAddr :: TNA -> TNA -> TNIO ()
tneSetRightAddr = undefined

tneSetTopAddr   :: TNA -> TNA -> TNIO ()
tneSetTopAddr = undefined

