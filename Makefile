
test03::
	./dumpTNETypeBits.sh > TNETypeBits.hs
	ghc --make test03.hs

all:
	#gcc -m64 -nostdlib -static -Wl,-Ttext,0 -Wl,--build-id=none forthLike.S
	gcc -m64 -c forthLike.S
	gcc -static -o test02 test02.c forthLike.S
	#ghc --make patchElf
	#objdump -D test02 | grep "48 c7 c7 ff 11 ff 00" | awk -F':' '{ print $$1 }' | sed 's,^ *,0x,' \
	#	| gawk --non-decimal-data '{ printf("%x\n", int($$1 / 32) * 32 ) }' > tmp/labels.txt
	#./patchElf tmp/labels.txt test02
	#cat test02.patched > test02
	#rm -f test02.patched
	#objdump -D test02 | less

clean:
	rm -f *.s *.o *.hi *.chi *.chs.h a.out patchElf test02 test03 unique *_hsc_make*



