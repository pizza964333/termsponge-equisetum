#!/bin/sh

tmp=`mktemp -u --suffix DUMP_STETYPES_SH`

cat > $tmp.c <<EOF
#include <stdio.h>
#include "stackElement.h"

void main()
{
EOF

awk -F '[[:space:],]' '/^typedef enum/ { start = 1 } /^\} STEType/ { start = 0 } { start *= 2; if (start>2) print $2 }' < stackElement.h \
	| grep -E '^STE_[A-Z0-9_]+$' | awk '{ print "  printf(\"%s %d\\n\",\"" $0 "\" ," $0 ");" }' >> $tmp.c

cat >> $tmp.c <<EOF
}
EOF

gcc -I. -o $tmp $tmp.c
$tmp
rm -f $tmp.c $tmp


