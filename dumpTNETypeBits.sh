#!/bin/sh

tmp=`mktemp -u --suffix DUMP_TNETYPES_SH`

cat > $tmp.c <<EOF
#include <stdio.h>
#include "termNetElement.h"

void main()
{
EOF

awk '{ printf("%08d %s\n",NR,$0) }' < termNetElement.h  | sort -r | sed 's,^.........,,' > $tmp.00

awk -F '[[:space:],]' '/^typedef enum/ { start = 0 } /^\} TermNetElementTypeBinar/ { start = 1 } { start *= 2; if (start>2) print $2 }' < $tmp.00 \
	| grep -E '^TNE_[A-Z0-9_]+$' | awk '{ print "  printf(\"%s %d 2 0\\n\",\"" $0 "\" ," $0 ");" }' >> $tmp.c

awk -F '[[:space:],]' '/^typedef enum/ { start = 0 } /^\} TermNetElementTypeUnar/ { start = 1 } { start *= 2; if (start>2) print $2 }' < $tmp.00 \
	| grep -E '^TNE_[A-Z0-9_]+$' | awk '{ print "  printf(\"%s %d 4 1\\n\",\"" $0 "\" ," $0 ");" }' >> $tmp.c

awk -F '[[:space:],]' '/^typedef enum/ { start = 0 } /^\} TermNetElementTypeValue/ { start = 1 } { start *= 2; if (start>2) print $2 }' < $tmp.00 \
	| grep -E '^TNE_[A-Z0-9_]+$' | awk '{ print "  printf(\"%s %d 8 3\\n\",\"" $0 "\" ," $0 ");" }' >> $tmp.c

cat >> $tmp.c <<EOF
}
EOF

echo "module TNETypeBits where"
echo

gcc -I. -o $tmp $tmp.c
$tmp | awk '{ printf("_%s = %d\n", $1, $2 * $3 + $4) }'
$tmp | awk '{ printf("typeBitsToString %d = \"%s\"\n", $2 * $3 + $4, $1) }'
rm -f $tmp.c $tmp


