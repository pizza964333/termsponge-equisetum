
#include <stdint.h>

typedef enum {

	STE_UNUSED = 0,

	STE_INT32,

	STE_JS_LOGIC, // True False Undefined Null
	STE_JS_SYMBOL,
	STE_JSON,
	STE_JS_OBJECT,

	STE_STRING,   // ascii or UTF-8
	STE_REGEX,
	STE_STACK_ARRAY,
	STE_ITERATOR,

	STE_JS_ERROR,
	STE_STRING_ERROR,
	STE_RUBY_SYMBOL,
	STE_RUBY_SYMBOL_ERROR,

	STE_UINT32,
	STE_UINT32_NEG,
	STE_UINT64,
	STE_UINT64_NEG,
	STE_UINT128,
	STE_UINT128_NEG,
	STE_UINT256,
	STE_UINT256_NEG,
	STE_UINT512,
	STE_UINT512_NEG,
	STE_UINT1K,
	STE_UINT1K_NEG,
	STE_UINT2K,
	STE_UINT2K_NEG,
	STE_UINT4K,
	STE_UINT4K_NEG,

	STE_FLOAT32,
	STE_FLOAT64,
	STE_FLOAT128,
	STE_FLOAT256,
	STE_FLOAT512,
	STE_FLOAT1K,
	STE_FLOAT2K,
	STE_FLOAT4K,

	// atticA some bits used for array size or number of 64 bit limbs or size of float

	STE_MPF_STRUCT,
	STE_MPZ_STRUCT,
	STE_MPZ_STRUCT_NEG,

	STE_BIT_ARRAY,
	STE_BYTES_ARRAY,
	STE_U64_ARRAY, // compatible with MPZ_STRUCT

	// U64 zero bits truncated and we get bytes
	STE_HASH_AS_BYTES_ARRAY,
	STE_SIGNATURE_AS_BYTES_ARRAY,
	STE_PUBLIC_KEY_AS_BYTES_ARRAY,
	STE_PRIVATE_KEY_AS_BYTES_ARRAY,

	STE_HASH_AS_U64_ARRAY,
	STE_SIGNATURE_AS_U64_ARRAY,
	STE_PUBLIC_KEY_AS_U64_ARRAY,
	STE_PRIVATE_KEY_AS_U64_ARRAY,

	// atticA used to store some bits for this structs
	STE_SET_OF_STE,
	STE_BAG_OF_STE,
	STE_MAP_OF_STE,
	STE_SET_OF_STE_SKIPLIST,
	STE_MAP_OF_STE_SKIPLIST,

	STE_DOUBLE_LINKED_LIST_OF_STE, // 24 bit links,     left right
	STE_BINARY_NET_OF_STE,         // 24 bit links, top left right

	// atticA used to store some bits about matrix size 
	STE_MATRIX_OF_STE,
	STE_SQUARE_MATRIX_OF_STE,
	STE_DIAGONAL_SYMMETRIC_MATRIX_OF_STE,
	STE_VERTICAL_SYMMETRIC_MATRIX_OF_STE,

	STE_MONOID_BASED_ON_ADDITION_AND_PERMUTATION_PERM001, // bit size 256, addition of 2^4 after permutation

	STE_HASH_STATE_FOR_SHA256,
	STE_HASH_STATE_FOR_SHA512,

	STE_QUATRNION_OF_STE,
	STE_SPLIT_QUATRNION_OF_STE,
	STE_COMPLEX_OF_STE,
	STE_SPLIT_COMPLEX_OF_STE,
	STE_DUAL_OF_STE,
	STE_DUAL_COMPLEX_OF_STE,
	STE_DUAL_TRIPLE_OF_STE_COMMUTATIVE_ASSOCIATIVE,
	STE_DUAL_TRIPLE_OF_STE_NONCOMMUTATIVE_ASSOCIATIVE,
	STE_BI_COMPLEX_OF_STE,

	STE_UNARY_TERM_MULT_INVERT_FOR_STE,
	STE_UNARY_TERM_MATRIX_INVERT_FOR_STE,
	STE_UNARY_TERM_ADDITIVE_NEGATE_FOR_STE,
	STE_UNARY_TERM_MULT_ELLIPTIC_IMAGINARY_FOR_STE,
	STE_UNARY_TERM_MULT_PARABOLIC_IMAGINARY_FOR_STE,
	STE_UNARY_TERM_MULT_HYPERBOLIC_IMAGINARY_FOR_STE,

	STE_BINARY_TERM_COMMUTATIVE_ASSOCIATIVE_MULT_CONJ_FOR_STE,
	STE_BINARY_TERM_COMMUTATIVE_ASSOCIATIVE_MULT_DISJ_FOR_STE,
	STE_BINARY_TERM_NONCOMMUTATIVE_ASSOCIATIVE_MULT_MONOID_FOR_STE,




} STEType;

typedef struct __attribute__((packed)) {
	union  __attribute__((packed)) {
		struct {
			struct {
			uint32_t kind       : 3;
			uint32_t type       : 9;
			uint32_t atticA     : 20;
			};
			struct {
			uint32_t smallValue : 4;
			uint32_t atticB     : 28;
			};
		};
		struct {
			uint64_t skip01   : 12;
			uint64_t bigValue : 52;
		};
		struct {
			uint32_t nonValue;
			union {
				uint32_t  uintV;
				int32_t    intV;
				float    floatV;
			};
		};
	};
} StackElement;

typedef uint64_t mp_limb_t;

// mpz struct
// kind = 7
// type = STE_MPZ_STRUCT * 2 + 1
// mp_alloc 10 bits limbs allocated
// mp_size  10 bits limbs used
// mp_limb_t *_mp_d 32 bit pointer difference with stack element address










