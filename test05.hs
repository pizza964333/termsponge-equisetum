
import TermNetElement
import TNETypeBits
import Control.Monad.IO.Class

main = do
  pool01 <- newTermNetPool
  runTNIO pool01 $ do
    a <- newTermNetElement _TNE_BINAR_OP_ASSOC_MULT_CONJ_COMM
    b <- tneGetTypeString a
    liftIO $ print (a,b)
