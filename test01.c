
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "stackElement.h"

uint32_t jumpTable(StackElement *ste)
{
	switch (ste->kind) {
		case 0: call_POINTER(); break;
		case 7:
		switch (ste->type) {
			case STE_UINT32:     call_UINT32();     break;
			case STE_INT32:      call_INT32();      break;
			case STE_FLOAT32:    call_FLOAT32();    break;
			case STE_MPZ_STRUCT: call_MPZ_STRUCT(); break;
		}
		break;
	}
}

int main(int argc, char *argv[])
{
	StackElement *a;
	StackElement *b;
	posix_memalign((void**)&a,65536*256*16,sizeof(StackElement));
	posix_memalign((void**)&b,1,sizeof(StackElement));
	uint8_t c = 0;
	((uint64_t*)a)[0] = 0;
	printf("sizeof(StackElement) = %d\n", sizeof(StackElement));
	printf("pointer  %p\n", a);
	printf("nonValue %p\n", &(a->nonValue));
	printf("uintV    %p\n", &(a->uintV));
	printf("zero data                            0x%016llX\n", ((uint64_t*)a)[0]);
	a->kind = 7;
	printf("set kind to 7                        0x%016llX\n", ((uint64_t*)a)[0]);
	a->type = 511;
	printf("set type to 511                      0x%016llX\n", ((uint64_t*)a)[0]);
	a->atticB = 268435455;
	printf("set atticB to 2^28-1                 0x%016llX\n", ((uint64_t*)a)[0]);
	a->atticA = 1048575;
	printf("set atticA to 2^20-1                 0x%016llX\n", ((uint64_t*)a)[0]);
	((uint64_t*)a)[0] = 0;
	a->uintV = 4294967295;
	printf("reset and set uintV to 2^32-1        0x%016llX\n", ((uint64_t*)a)[0]);
	a->bigValue = 4503599627370495;
	printf("set bigValue to 2^52-1               0x%016llX\n", ((uint64_t*)a)[0]);
	printf("show pointer to 64 bit stack element 0x%016llX\n", (uint64_t)b);
	printf("just pointer                         0x%016llX\n", (uint64_t)(&c));
	return 0;
}

