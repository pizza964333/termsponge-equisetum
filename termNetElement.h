
#include <stdint.h>
#include "stackElement.h"

/* six posible values
 * 0b010 0b011 0b100 0b101 0b110 0b111
 *     2     3     4     5     6     7
 *  addi  mult  addi  mult  addi  mult
 *  disj  disj  conj  conj  conj  conj
 *              ncom  ncom
 */

typedef enum {
	TNE_BINAR_OP_ASSOC_ADDI_DISJ_COMM = 2,
	TNE_BINAR_OP_ASSOC_MULT_DISJ_COMM,
	TNE_BINAR_OP_ASSOC_ADDI_CONJ_NCOM,
	TNE_BINAR_OP_ASSOC_MULT_CONJ_NCOM,
	TNE_BINAR_OP_ASSOC_ADDI_CONJ_COMM,
	TNE_BINAR_OP_ASSOC_MULT_CONJ_COMM,
} TermNetElementTypeBinar;

typedef enum {
	TNE_UNAR_OP_OFCOURSE,
	TNE_UNAR_OP_WHYNOT,
	TNE_UNAR_OP_MULT_INVERT,
	TNE_UNAR_OP_MATRIX_INVERT,
	TNE_UNAR_OP_ADDI_NEGATE,
	TNE_UNAR_MARKER_REDUCED_BY_ATOM,
} TermNetElementTypeUnar;

typedef enum {
	TNE_VALUE_TERM_ROOT_MULT_UNIT,
	TNE_VALUE_MULT_UNIT,
	TNE_VALUE_ADDI_UNIT,
	TNE_VALUE_ATOM_PACKET_ASSOC_MULT_CONJ_COMM,
	TNE_VALUE_ATOM_PACKET_ASSOC_MULT_CONJ_NCOM,
} TermNetElementTypeValue;

typedef union  __attribute__((packed)) {

	struct __attribute__((packed)) {
		uint64_t isNotBinary      : 1; // this bit can be checked at any time
		uint64_t isNotUnary       : 1; // we are grented to check this bit only if isNotBinary = 1
		uint64_t isForthLike      : 1; // we are granted to check this bit only if isNotBinary = 1 and isNotUnary = 1
		uint64_t isMultiplicative : 1; // we are granted to check this bit only if isNotBinary = 0
		uint64_t isCommutative    : 1; // we are granted to check this bit only if isNotBinary = 0
		uint64_t isConjunction    : 1; // we are granted to check this bit only if isNotBinary = 0
	} bit;

	struct __attribute__((packed)) {
		uint64_t skip  :  1;
		uint64_t type  :  3; // if isNotBinary is 0 than first 2 bits of typeA must has no less than one bit set, in this case we get 6 type values
		uint64_t top   : 20;
		uint64_t left  : 20;
		uint64_t right : 20;
	} binar;

	struct __attribute__((packed)) {
		uint64_t skip  :  2;
  		uint64_t type  :  6; // we are granted to check this bits only if isNotUnary = 0
		uint64_t attic : 16; // can be used by annonated unary operators, attic is link in this case and address of attic element must be aligned to 16
		uint64_t top   : 20;
		uint64_t right : 20;
	} unar;

	StackElement ste;            // we are granted to use this field only if isForthLike = 1

	struct __attribute__((packed)) {
		uint64_t skip  :  3;
		uint64_t type  : 13; // we are granted to check this bits only if isForthLike = 0
		uint64_t data  : 48;
	} value;

} TermNetElement;








