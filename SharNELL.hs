module SharNELL where

import Data.Int
import Data.Word
import Foreign.Marshal.Array
import Foreign.Storable
import Foreign.Ptr
import System.IO.Unsafe
import Control.Monad.Trans.Reader
import TNETypeBits

data Term = Atom Int16
          | Inve Term
          | Favo Term
          | Term `Times`  Term
          | Term `Concat` Term
          | OfCourse Term
 deriving (Read,Show,Eq)

data TermNetElement

newtype TermNetAddr = TNA Int

type TNA = TermNetAddr

instance Storable TermNetElement where
  sizeOf    _ = 8
  alignment _ = 8

data TermNet = TermNet { start :: Ptr TermNetElement, entry :: [TermNetAddr] }


newTermNet :: IO TermNet -- 8 Megabytes will allocated
newTermNet = do
  ptr <- callocArray $ 2^20
  return $ TermNet ptr []


termToNet :: TermNet -> Term -> TermNetAddr
termToNet termNet (a `Times` b) = unsafePerformIO $ (\a -> runReaderT a termNet) $ do
  let c = termToNet termNet a
  let d = termToNet termNet b
  e <- newTermNetElement _TNE_BINAR_OP_ASSOC_MULT_CONJ_COMM
  e `tneSetLeftAddr`  c
  e `tneSetRightAddr` d
  c `tneSetTopAddr`   e
  d `tneSetTopAddr`   e
  return e

type TNIO a = ReaderT TermNet IO a

newTermNetElement :: Integer -> TNIO TermNetAddr
newTermNetElement = undefined

tneSetLeftAddr  :: TNA -> TNA -> TNIO ()
tneSetLeftAddr = undefined

tneSetRightAddr :: TNA -> TNA -> TNIO ()
tneSetRightAddr = undefined

tneSetTopAddr   :: TNA -> TNA -> TNIO ()
tneSetTopAddr = undefined











  
