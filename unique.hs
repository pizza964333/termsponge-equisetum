
import System.IO
import Data.List
import Data.Char

main = do
  a <- getContents
  let b = map words $ lines a
  let c = unique b
  let d = sortBy (\a b -> conv a `compare` conv b) c
  let e = reverse $ foldr j [] $ reverse $ map f d
  putStr $ unlines $ map (\((a,b),c) -> unwords [a,b,show c]) e

unique list = nubBy (\a b -> take 2 a == take 2 b) list

conv a | all isDigit (a !! 2) = read (a !! 2) :: Int
conv _ = 999

f o@(a:b:c) = ((a,b),conv o)

j (a,999) c@((_,n):b) = (a,n+1) : c
j (a,999) b = (a,0) : b
j a b = a : b
