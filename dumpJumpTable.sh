#!/bin/sh

tmp=`mktemp -u --suffix DUMP_JUMP_TABLE`

cat > $tmp.00

cnt=`wc -l $1 | awk '{ print \$1 }'`
for i in `seq 1 $cnt`; do
	cat $1 | awk -F'[[:space:]]*;[[:space:]]*' "NR == $i { print \$1 }" > $tmp.01
	cat $1 | awk -F'[[:space:]]*;[[:space:]]*' "NR == $i { print \$2 }" > $tmp.02
	cat $1 | awk -F'[[:space:]]*;[[:space:]]*' "NR == $i { print \$3 }" > $tmp.03
	a="`cat $tmp.01`"
	b="`cat $tmp.02`"
	c="`cat $tmp.03`"
	#echo "==== " "$a" "$b" "$c"
	grep -E "$a" $tmp.00 | awk '{ print $1 }' > $tmp.04
	grep -E "$b" $tmp.00 | awk '{ print $1 }' > $tmp.05
	for p in `cat $tmp.04`; do
		for q in `cat $tmp.05`; do
			echo $p $q $c >> $tmp.06
		done
	done
done

cat $tmp.06 | ./unique


